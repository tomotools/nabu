#!/bin/bash

git fetch origin master:tmp_ci_master

CURRENT_BRANCH="$(git rev-parse --abbrev-ref HEAD)"
VERSION_STR="$(grep 'version' 'nabu/__init__.py' | head -1 | cut -d '=' -f 2)"
DIFFS_FILES="$(git diff --name-only $CURRENT_BRANCH tmp_ci_master -- nabu)"

if [ "$CURRENT_BRANCH" == "master" ]; then
	exit 0
fi

if [ -z "$DIFFS_FILES" ]; then
    # Likely master was cloned (gitlab uses detached head, so current branch is not master)
    exit 0
fi

DIFFS="$(git diff $CURRENT_BRANCH tmp_ci_master -- nabu/__init__.py)"

if [ -z "$DIFFS" ]; then
	echo "Please update the micro version number. Current version on master branch is $VERSION_STR"
	exit 1
else
	exit 0
fi
