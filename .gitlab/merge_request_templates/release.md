MR for major release 202X.Y.0


## Prior work

  - [ ] Update Documentation
    - [ ] Changelog
    - [ ] v2023_1_0.md
    - [ ] features.md
    - [ ] index.rst
    - [ ] Notebooks
    - [ ] APIDoc

## Release candidate

  - [ ] Bump version to alpha or RC
  - [ ] Upload the release candidate on pypi
  - [ ] Create a fresh new environment with this release
  - [ ] Non-regression tests (with RC) - use `automatix`
  - [ ] (Optional) invite "friendly-users" to test this RC
  - [ ] Fix possible bugs encountered in the process

## Release

  - [ ] Bump version to final
  - [ ] Build and deploy documentation
  - [ ] Create and push the tag: `git tag --sign -a v20XX.Y.Z && git push origin v20XX.Y.Z`
  - [ ] Deploy the final wheel on pypi

## Memo

### Wheels and pypi

To build the wheel and sources package:
```bash
python -m build
```

To upload it on testpypi:
```bash
 twine upload --repository testpypi mywheel.whl sources.tar.gz
```

### Create a fresh environment

With [cvmfs-conda](https://gitlab.esrf.fr/apptainer/tomotools-conda): push to the `main` or `dev` branch to trigger the CI

With automatix:

```bash
/scisoft/tomotools/automatix/deployment/deploy_locally.sh <env_name>
```

### Non-regression tests

Use [automatix](https://gitlab.esrf.fr/tomotools/automatix/-/tree/master/nonregression_tests) for non-regression tests.
For example:

```bash
/scisoft/tomotools/automatix/nonregression_tests/run_nonregression_test.sh cud10 /data/scisofttmp/paleo/nabu_end2end_tests/CuD10/rec_2022.2.0-rc5/CuD10_III_400C_compression02125umps_005__rec.hdf5
```