# Change Log

## Next version

### Application

  - Fix tilt correction crashing in some cases
  - Fix reconstruction from even/odd angles

### Library

  - Use numpy 2


## 2024.2.0

### Application

  - Allow to reconstruct multiple entries in the config file, using `hdf5_entry = *` or `hdf5_entry = entry0000, entry0001, entry0002` - !511
  - Add `method = HBP` reconstruction method: Hierarchical Back-Projector (faster than FBP, but approximate result). - !512
  - Add `outer_circle_value` (0 by default) to tune the value of the voxels falling outside the reconstruction region (when `clip_outer_circle` is enabled) - !462
  - Remove `rotate_projections` parameter, which is a duplicate of `tilt_correction` - !490
  - Fix radios rotation when reconstructing a single slice - !464
  - Improvements of the cone-beam reconstruction pipeline: - !478 , !484 , !486 , !488
    - Add support for advanced options: `filter_type`, `padding_mode`, `slice_roi` and horizontal translation
    - The output is now consistent, in terms of scaling and angular convention, with FBP
  - Add `rotation_axis_position = vo`, a new center of rotation estimator from [algotom](https://github.com/algotom/algotom) - !465
  - Stitching: (!429 , !491 , !505 , !519)
    - Generalize pre-processing stitching. Create dedicated stitching for y-stitching on NXtomo(s).
    - Rework calculation of final position
  - Add `nabu-config --overwrite` parameter - !463
  - Add `method = MLEM` iterative reconstruction in configuration file, in `[reconstruction]` section - !504
  - Also  `implementation` parameter in the `[reconstruction]` section of the config file. It will serve to pick between different
implementations of reconstruction methods. For example MLEM defaults on "corrct" - !504

### Library

  - Add `nabu.reconstruction.hbp.HierarchicalBackprojector`: cuda implementation of hierarchical-backprojector by Jonas Graetz ( Fraunhofer Institute) and Alessandro Mirone (ESRF) - !512
  - (Pending deprecation) In a future version, all the center of rotation estimators in `nabu.estimation` will return the center of rotation relative to the left of the image by default (instead of relative to the middle). To recover previous behavior, set `return_relative_to_middle=True` in the `find_shift()` method. - !507
  - Make VKFFT the default implementation used for cuda FFT. You can use `NABU_FFT_BACKEND="skcuda"` or `NABU_FFT_BACKEND="vkfft"` to choose which FFT to use. - !506
  - Remove `io.writer.EDFWriter`, `io.writer.TIFFWriter` and `io.writer.JP2Writer` - those are now handled by tomoscan. `preproc.double_flat_field` now only supports exporting/importing to/from HDF5. - !426
  - Remove `margin` parameter in `PaganinPhaseRetrieval` class - this parameter is never used within the class - the processing margin is handled by the reconstruction pipeline. - !427
  - Cone-beam: use same angular convention as nabu FBP - !478
  - Cone-beam: many improvements: add support for horizontal shifts ("axis_correction"), scale factor, ROI reconstruction and custom filtering. - !484
  - Add `io.reader.NXTomoReader` and `io.reader.EDFStackReader` for more efficient data reading - !430 , !439
  - `preproc.flatfield.FlatFieldDataUrls` is no more used in the processing pipeline. This class is now deprecated. !489
  - Automatically switch to vkfft if scikit-cuda is not available, for Cuda FFT - !506



## 2024.1.0

### Application

  - The full-field pipeline should use less GPU memory. To reconstruct using VKFFT rather than CUFFT (saving a noticeable amount of memory, use `NABU_FFT_BACKEND=vkfft nabu nabu.conf`) - !378
  - Add two sinogram-based rings correction: vo, mean-subtraction, mean-division - !375
  - Add two new center of rotation estimation methods: `fourier-angles` and `octave-accurate` - !298
  - Center of rotation estimation now automatically uses the coarse estimate from motor as a starting point, for method `composite-coarse-to-fine` (aka `near`) and `sliding-window`.
  - Fix `nabu-multicor` producing extraneous folders for HDF5 output format - !363
  - `normalize_srcurrent` is now set to 1 by default - images will be normalized with synchrotron current if such metadata is found.
  - Add two modes for `exclude_projections` (!360):
    - `exclude_projections = angles=angles_file.txt` where `angles_file.txt` contains one angle value (in degrees) per line
    - `exclude_projections = angular_range=[1,2]` will discard all projections in the angular range `[a, b]` in degrees
  - Fix SRCurrent normalization never taking place because flats SRCurrent were not read properly
  - Enable to reconstruct from even and odd projections. In the `[dataset]` section, use `projections_subsampling=2:0` (resp. `projections_subsampling=2:1`) to reconstruct from even (resp. odd) projections. !370
  - Stitching: Add normalization by region-of-interest: !369
  - Fix crash when using `flatfield=force-compute` if files already exist - !355
  - Fix double flatfield + when using a filtering (`dff_sigma`) with projections/flats values that can be smaller than dark - !395
  - Reconstruction: `centered_axis` is now set to True by default.

### Library

  - `CudaBackprojector` now supports `use_textures=False`. Cuda 12 removed textures references, and pycuda does not support the new texture objects. So for Cuda 12, textures can't be uses: !337
  - Add VKFFT implementation for cuda backend, it can be used with `NABU_FFT_BACKEND=vkfft` - !273
  - Add OpenCL backend for FFT, based on VKFFT (`nabu.opencl.fft`) - !361
  - Add fftshift OpenCL implementation - !362
  - Add out-of-place transpose for Cuda and OpenCL - !357
  - Add `nabu.processing` modules. Many general-purpose classes are moved to this module. !364
  - `CudaUnsharpMask` and `OpenCLUnsharpMask`: API changed to pass directly the cuda/opencl options directly in kwargs
  - Add Nghia Vo's implementation of sinogram de-striping method (from algotom): `nabu.reconstruction.rings.VoDeringer`, and its cuda implementation (from tomocupy): `nabu.reconstruction.rings_cuda.CudaVoDeringer`
  - Add `nabu.reconstruction.rings.SinoMeanDeringer` and `nabu.reconstruction.rings_cuda.CudaSinoMeanDeringer`
  - Backprojector: Fix abrupt position rounding when using `centered_axis=True`. Also fix interpolation issue when not using textures. !384


## 2023.2.0

### Application

  - Volumes casting (`nabu-cast`): add `--compression-ratios` and `--histogram-url`:  !295
  - Volumes stitching (`nabu-stitching`): !232, !297, !300, !305, !327
    - Extend stitching to reconstructed volume.
    - Add first implementation of "image minimum divergence" and "higher signal" algorithms.
    - Various life improvement from first feedbacks
  - Add tool to compute reduced darks/flats: `nabu-reduce-dark-flat`: !324
  - Add a tool to reconstruct the same slice(s) with varying centers of rotation (for visual CoR determination) `nabu-multicor`: !318


### OpenCL support

Several components now have an OpenCL backend: !245, !310, !328.
This entailed a refactoring that does not break the API.

Each processing class (eg. `SinoFilter`) now has
  - a base class (code factorization + reference numpy implementation),
  - a cuda backend (eg. `CudaSinoFilter`)
  - an opencl backend (eg. `OpenCLSinoFilter`).

The wrapping to Cuda and OpenCL are done thanks to pycuda and pyopencl, respectively.
Usually, large chunks of code can be factored into the base classes, thanks to the similarity of the API of pycuda and pyopencl.

  - General-purpose classes: `OpenCLKernel`, `OpenCLProcessing`, `OpenCLMemcpy2D`
  - Reconstruction-related classes: `OpenCLSinoMult`, `OpenCLPadding`, `OpenCLSinoFilter`, `OpenCLBackProjector`
  - Processing: `OpenCLUnsharpMask`
  - Utilities functions in nabu.opencl.utils: `get_opencl_devices()`, `allocate_texture()`, `check_textures_availability()`, `copy_to_texture()`


### Reconstruction

  - Half-tomography is now handled without performing any sinogram stitching: !306
    This solves issues related to odd-number-of-projections and center of rotation.
    Also, it's now possible to use vertical translations along with half-tomography.
    The debugging capability is unchanged as `save_steps = sinogram` is still possible in the configuration file.

  - Add forward projector, for now only with the Cuda backend: !270
    This enables using iterative reconstruction methods (parallel geometry) with persistent arrays on GPU.

  - Add a parameter `padding` for sinogram de-ringer (Fourier-Wavelets method): !329
    This helps to remove aliasing artefacts due to the periodic wavelets decomposition.


### Misc
  - Median filter now uses silx median filter instead of scipy, leading to some speed-up: !291
  - Fix `alignment.translation` for tilt estimation: !280
  - Fix writers and extraneous folders and missing metadata for non-HDF5 : !299, !303



## 2023.1.1

  - Fix alignment tilt: !280
  - Fix cast_volume: improve get_default_output_volume, make sure the Volume can generate a volume identifier: !285
  - Fix angles for legacy EDF datasets: !283


## 2023.1.0


### Application


The pipelines internally used by Nabu were completely rewritten to address many shortcomings:

  - When using half acquisition, don't duplicate data for radios and sinograms. This means that eventually, larger chunks of data can be processed at once, resulting in overall better performances.
  - Fix `binning_z` which was broken most of the times
  - Fix `subsampling` though it still won't yield better performances when using HDF5
  - Fix processing margin when using vertical translations and/or CTF
  - When using half acquisition, use the correct center of rotation (fix 0.5 pixel shift)
  - Flats distortion correction now works on the GPU backend, but is still very slow
  - Now use 6 digits for output files (partial HDF5 results and single files like tiff, edf)



This version also adds these major features:
  - Helical full-field pipeline: `nabu-helical` CLI and `nabu.pipeline.helical` API.
  - Start to support detector distortion correction (apply a mapping on each radio at the reading stage)
  - Volumes stitching (`nabu-stitching`), though still work in progress
  - Experimental support for cone-beam full volume reconstruction: `method = cone` in `[reconstruction]`


Other new minor features:
  - Allow to overwrite some metadata, ex. with `overwrite_metadata = energy=19kev; pixel_size = 1.6 micron` in `[dataset]`
  - Unsharp mask: add "imagej" mode to have an unsharp similar to the one of ImageJ
  - `binning` and `binning_z` can now be set to arbitrary (integer) values.
  - Add support for .vol output format (mimics the big binary file from PyHST2)
  - Configuration file now supports relative paths


New commands:
  - `nabu-stitching`: volumes stitching. Configuration file can be bootstraped with `nabu-stitching-config`
  - `nabu-helical`: reconstruction of acquisition done with helical trajectory. Configuration file can be bootstraped with `nabu-config --helical`
  - `nabu-shrink-dataset`: perform binning and/or subsampling on a NX file, updating all relevant metadata. Might be moved to `nxtomomill` in the future
  - `nabu-composite-cor`: estimate the center of rotation on a scan (or series of scans) using the composite (radios+sinos) CoR estimator


### Library

  - Many deprecated parameters were removed in various functions and classes
  - Rework `SinoBuilder` API. Use `get_sino()` and `get_sinos()`.
  - Fix operations on large (> 17 GB) cuda arrays


## 2022.2.0

### Application

  - Add Hilbert filter (`fbp_filter_type`) for differential backprojection.
  - Add configuration files templates (`nabu-config --template XX`)
  - Add option to normalize by Synchrotron current (`normalize_srcurrent`).
  - Add `nabu-compare-volumes` CLI tool
  - Add custom sinogram normalization from the configuration file (subtract/divide by a user-provided image)
  - Add volume casting application with rescale capability ("nabu-cast").
  - Improve the result of the "composite-sino-coarse-to-fine" center of rotation estimator (aka "near").
  - Improve handling of flats/darks for more flexibility (`darks_flats_dir`)
  - Fix horizontal/vertical translations in pipeline
  - Fix: improve handling of user-defined values through text files (ex. angles_files)


### Library

  - Fix NaN support in flat-field
  - Add support for cone-beam reconstruction (partial volume slabs). For now it's only available from the API, not the configuration file.
  - Increasingly, loading/saving volumes (all formats) will be backed by tomoscan.esrf.volume API.


## 2022.1.0

### Application

  - Reconstruction is now possible without GPU. The "CPU reconstruction" is now a fall-back when no GPU is available.
  - Added new FBP filters
  - Some usability improvements:
    - `rotation_axis_position` is now set to `sliding-window` by default. In half tomography, the option `side="right"` is automatically set.
    - `overwrite_results` is set to `True` by default.
    - `padding_mode` is set to `edges` for FBP by default.


### Library

  - `CTFPhaseRetrieval` now takes all the parameters in initiation (rather than "run-time").
  - Add FFTW support for `CTFPhaseRetrieval`
  - `PaganinPhaseRetrieval`: rename `use_R2C` to `use_rfft` to be consistent with the other functions
  - `PaganinPhaseRetrieval`, `CTFPhaseRetrieval` and `utils.get_num_threads()` now use `-1` to use all threads (previously it returned all available threads except one).

## 2021.2.1

Minor version with several changes.

### Application

  - Loading darks/flats from a dataset is now done with files `darks.h5` and `flats.h5` if available.
  - Fixed HDF5 overwriting behavior
  - Pipeline: don't write processing options in each individual chunk file. This saves some time when writing data.

### Library

  - Removed 3D backprojection. This means that from now on, only 2D sinogram can be backprojected.


## 2021.2.0

This version brings major changes in the API to make it more consistent. Most of the pre-2021.2.0 API should work, although possibly with a deprecation warning. This version also brings a lot of fixes in the full-field pipeline.

### Application

  - Half tomography does not require an even number of angles anymore
  - Half tomography reconstruction: change the way to weight the two half sinograms in the overlapping region. This only impacts the center of the reconstructed slice. The new method is compatible with other existing tomography softwares.
  - Fix chebyshev normalization on sinograms for half tomography
  - EDF datasets: fix long loading time (assume 1 frame per file by default)
  - Added composite sinogram/radio Center of Rotation estimator
  - The new pipeline (handling full radios) has now a Cuda backend, except for distortion correction.
  - Fix `enable_halftomo = auto` not working when FOV == "full"
  - Renamed configuration key 'flatfield_enabled' to 'flatfield'. The following modes are available:
     - False: disable flatfield normalization
     - True: attempt at loading "reduced" flats/darks, compute if no file available
     - "force-compute": discard any existing file with pre-computed flats/darks, do the computations anyway
     - "force-load": Force to use existing flats/darks from a file, regardless of the input dataset
  - Unknown configuration keys are now ignored by default (instead of making nabu exit with error)
  - Add support for big tiff output format (all slices written in one single tiff file)
  - Fix support for Cuda 11, notably context creation/disposal
  - FBP: add "centered_slice" so that the slice is centered on the rotation axis
  - FBP: add "clip_outer_circle" to set to zero values outside the reconstruction region


### Library

  - Create `nabu.pipeline` which will contain all pipeline implementations (full-field, XRD-CT, ...). Move modules (ex. from `nabu.resources`).
  - Rename pipelines with more consistent names: `Chunked` and `Grouped`.
  - `nabu.preproc`: `sinogram` and `rings` were moved to `nabu.reconstruction`
  - Add `nabu.preproc.phase_cuda.CudaCTFPhaseRetrieval`
  - Add `nabu.cuda.binning` which offers a generic Cuda padding utility
  - Use the `tifffile` python module instead of `silx.third_party.TiffIO` for supporting big-tiff format


## 2021.1.1

### Application

   - Added `unsharp_method` to select unsharp mask method (gaussian or log)
   - `enable_halftomo` can now be set to `auto` to let nabu determine if half aqcuisition was used
   - Add `nabu-validator` command-line tool

### Library

   - `scipy` is now a mandatory dependency


## 2021.1.0

### Application

  - Add sinogram-based center of rotation estimation methods.
  - Add projections rotation and detector tilt estimation.
  - Add support from dumping processing steps into a file, and to resume a pipeline from a saved step.
  - Add sinogram-based rings artefacts removal method
  - Add  CTF single-distance phase retrieval
  - Add support for flats distortion estimation and correction
  - Add EDF output data format

### Command-line interface tools

  - Add `nabu-rotate` for performing a rotation of each projection in a dataset.
  - Add `nabu-double-flatfield` to compute the double flatfield image from projections.

## Library

  - `PaganinPhaseRetrieval` length parameters are now expressed in meters.
  - Add `nabu.estimation` module
  - The `preproc.FlatField` class now takes arrays as input instead of `DataUrl`. The class `FlatFieldDataUrls` can be used as a replacement.

## 2020.5.0

Major version (November 2020).

### Application

  - Add cuda backend for histogram, also for the `nabu-histogram` command.
  - Enable histogram computation for other output formats than HDF5 (ex. tiff)
  - The files "nabu_processes.h5" and "nabu.log" are now named as a function of the dataset prefix. This prevents conflicts when several datasets are reconstruction simultaneously from the same folder.
  - Add nabu-generate-info CLI tool, which generates a ".info" for pluggin ESRF legacy pipeline post-processing tools.
  - Add a utility to exclude projections from being processed: `exclude_projections` in `[dataset]`
  - Add new methods for finding the center of rotation
  - Mitigate issue of nabu hanging forever to allocate memory on big data chunks
  - Fix unsharp mask when used without phase retrieval
  - When using HDF5 datasets, a custom entry in the file can now be specified using the `hdf5_entry` key in the `[dataset]` section in nabu configuration.


### Library

  - Fix half-tomography sinograms when center of rotation is on the left
  - Add misc.histogram_cuda: Cuda backend for partial volume histograms
  - Add `preproc.alignment.CenterOfRotationAdaptiveSearch`, `preproc.alignment.CenterOfRotationSlidingWindow` and `preproc.alignment.CenterOfRotationGrowingWindow` for automatic CoR estimation.
  - Add support for `h5py >= 3.0`, although it requires `silx >= 0.14`


## 2020.4.2
This version brings new features and fixes.

### Application

  - Center of Rotation is not incorrectly shifted anymore when set on the left in "normal acquisition mode"

## 2020.4.1

This is a minor version bringing some fixes in the application.

### Application

  - In the HDF5-NX dataset file, the pixel size taken by nabu is now `magnified_pixel_size` instead of `pixel_size`. This will have notable consequences for phase retrieval (`delta_beta` will be much smaller to obtain the same effect).
  - The `nabu-config` command line does not need the `--bootstrap` flag anymore, as it is not the default behavior.
   - Configuration file: angles in `angles_files` have to be provided in degrees (as for `angles_offset`)
   - Fixed some comments in configuration file
   - A more helpful error will be returned if either flat/dark is missing from NX file.


## 2020.4.0

This is a version adds a number of features.

### Application

  - Automatic center of rotation estimation, working for both half-acquisition and regular scans
  - Command line tool for splitting a NXTomo file by "z" series
  - Volume histogram. Command line tool for merging histogram of multiple volumes.
  - Enable to perform flat-field normalization with darks/flats from another dataset
  - Sinogram normalization (baseline subtraction)
  - Nabu does not need `tomwer_processes.h5` to get the "final" darks/refs anymore.
  - Fix `fbp_filter_type = none`: now it will actually disable any filtering
  - Fix auto-CoR not working when flatfield is disabled

### Library

  - Add `misc.histogram` for computing partial histograms and merging them
  - Add `preproc.sinogram.SinoNormalization`
  - Fix double flat-field when `dff_sigma > 0`  which was giving nonsense results.
  - Half-tomography: add support for center of rotation on the left side

## 2020.3.0

This is a release for ESRF User Service Mode restart.

The main new feature is the full volume reconstruction. The reconstruction is done in multiple stages by dividing the volume in  sub-volumes, which are processed and stitched together in the end. 

### Application

  - Multi-stage volume reconstruction with possibly overlapping chunks (`LocalReconstruction` in `nabu.app`).  Chunk sizes are computed automatically as a function of available memory. Phase margin is computed automatically as a function of phase retrieval parameters. HDF5 files corresponding to each chunk are merged as a HDF5 master file.
  - Basic support for tiff output format (float32, single frames).
  - Basic support for jpeg2000 output format (uint16, single frames). For now only lossless compression is supported. 
  - Integrate "CCD correction" (thresholded median filter) in pipeline
  - Command Line Interface improvements: `nabu nabu.conf --slice middle`,  add options `--cpu_mem_fraction` and `--gpu_mem_fraction`.

### Library

   - Add alignment utilities: `nabu.preproc.alignment`. There are many use cases, please see the corresponding documentation pages for more information.
   - Flat-field: support more than two flats
   - Unify API of `CCDCorrection` and `CudaCCDCorrection`


## 2020.2.0

This is a "preview release", preparing an important release foreseen for September 2020.

### Library features

  - Vertical translations
  - Double flat-field (radios-based rings artefacts removal)
  - Support for half tomography
  - `preproc.alignment` module
  - Support for flat-field normalization with more than 2 flats

### Application
  - Take configuration file `start_z` and `end_z` into account for `nabu` CLI
  - Phase retrieval: `method = none` is now default (no phase retrieval by default)
  - Add `overwrite_results` 
  - Add support for `rotation_axis_position = auto` (except for half-tomo)

### Internal
  - Re-wrote internal processing pipeline with a much simpler design
  - NXresults: config is now a H5 dataset. "image stack" default interpretation.


## 2020.1.0: 2020/04/29

This is the first official release of Nabu. Please see the [features overview](www.silx.org/pub/nabu/doc/features.html)
