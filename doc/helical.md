Helical Pipeline
----------------

The helical pipeline  is invoked with the following command, which takes as argument the name of a configuration file
```
    nabu-helical nabu_config_file.cfg
```

### Helical Specific Parameters


the config file is mainly the same as for the standard pipeline with few exceptions.
Here we list them, for each section of the configuration file 


- *preproc* section
   - **processes_file**
     this file is expected to contain the map of weights, of the same shape as the detector, and the double
     flatfield, which can possibly be equal to one everywhere, but is needed nonetheless. A default file can
     be created with the **nabu-helical-prepare-weights-double** command
     ```
     nabu-helical-prepare-weights-double  name_of_tomomilled_file.nx entry0000
     ```
     where the second argument is the entry name for desired scan. This command creates a file name *double.h5*.
     
      - example for setting the **processes_file** parameter in the preproc section
        ```````````````````````````````````
        processes_file =  map_and_doubleff.h5   
        ```````````````````````````````````

   - **double_flatfield_enabled = 1**
      always enable this. If you dont have any double-flat  provide as a fallback solution a
      completely flat double_field, everywhere equal to 1.

   - **flatfield=1**
     Always enable flat-field. If you dont have one provide a uniform flat-field.

- *reconstruction* section
   - **enable_halftomo**
     nabu-helical applies the same treatment for half-tomo as for full-tomo.
     Always let this key to zero
     ```
     enable_halftomo = 0
     ```
   - **start_z, end_z, start_z_mm, end_z_mm**
     these parameters determine the, vertically, reconstructed region.
     if they are not set, they take their default values, which triggers the reconstruction of the whole
     reconstructable region. Such default values are:
     ```
     start_z = 0
     end_z =   -1
     start_z_mm = 0
     end_z_mm =   0
     ```
     These parameters offer two alternative methods to specify the reconstruction range :
       - if both **start_z_mm** and **end_z_mm** are identical to zero, then the recontruction range
         is determined on the basis of **start_z** and **end_z**.
         Use positive integers, with **start_z** < **end_z**, strictly, unless you use the default values
	 which trigger the whole reconstruction.
         The reconstructed vertical region will be  performed form slice start = first doable + start_z
         until  slice end   = first doable + end_z. If necessary the range will be clipped, if the given region exceed the doable range.
	 example:
	 ```
	 start_z = 10
	 end_z = 20
	 ```
	 for reconstructing 10 slices close to scan start. NOTE: we are proceeding in the direction of the scan so that, in the
	 laboratory reference system
         the start may be above or below the end, depending on the direction of the scan.
       - If one of **start_z_mm** **end_z_mm** or both ar different from zero,
         the reconstruction range is the one comprised between these two values, interpreted as heights, in millimiters,
	 above the sample stage.
	 
    - **use_hbp**
      this parameter is by default set to **0**. If it is set to **1**, and IF the package is available
      (not  available in the public branch yet) the hyerarchical backpropagation algorithm (Jonatz Graetz 2021)
      is used.
      Default is :
      ```
      use_hbp = 0
      ```
    - **redundancy_angle_deg**
      this parameter has its role in the criteria used to establish wether a given slice is reconstructable or not.
      By default the minimal required span is  360 degree.
      When dead zones in the detector exists, instead,
      ( dead zones are tagged with zero weight in the weight field)
      the minimal required span is increased by 2*redundancy_angle_deg.
      Default is :
       redundancy_angle_deg = 0
       
    - **angular_tolerance_steps**
      the angular tolerance, an angular width expressed in units of an angular step, which is
      tolerated in the criteria for deciding if a slice is reconstructable or not.
      Its defult is zero. This tolerance can be used to reconstruct non helical scan which cover slightly less then 360 degrees.


### Concerning the output
 if tiff format is chosed, the slices will be written separately, one per file, and the name of the
 file will be postpended, just before the file extension (*.tiff*), with the height in millimiters
 that the  slice has above the sample stage. This value is written with a fixed number of digits,
 with the dot of the floating point number replaced by the letter *p*. The total lenght of the file name
 being thus constant for all the slices.
 
### Full list of helical specific parameters.

we have given here the most commonly used parameters. The full list of available parameters, comprising also the one very seldomly used,
can be found in the file *nabu/pipeline/helical/nabu_config.py*




