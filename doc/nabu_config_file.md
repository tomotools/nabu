# Configuration file

## Rationale

A tomography processing can differ notably from one dataset to another: how should the data be read (file format, binning, ...) ? What are the preprocessing steps (phase retrieval, CCD hotspots, ...) ? Do we apply some corrections (rotation axis, rings artefacts, ...) ? In which region of interest do we want to reconstruct ?

For this reason, Nabu enables to describe the processing steps in a configuration file. The nabu configuration file ("`nabu.conf`") is therefore a file describing the processing steps, from dataset analysis/preprocessing to the reconstruction. It is the equivalent of PyHST2 "`.par`" files, although sometimes with a different approach.

## Getting started

Once nabu is installed, you can create a configuration file from scratch with the tool `nabu-config`:
```bash
nabu-config
```
More options are available, see `nabu-config --help`.

This bootstrap command will create a new file (`nabu.conf` by default) in the current directory. You can then edit the relevant fields to describe the processing you want.

All the configuration options are pre-filled with their default value, except the dataset location.

## Options visibility levels

By default, the configuration file is generated with the "most usual" options. In order to add more options, you can tune the `--level` parameter:
  - `nabu-config --level required` (default) generates a "minimalistic" configuration file
  - `nabu-config --level optional` generates the configuration file with more options
  - `nabu-config --level advanced` generates the configuration file with all options


## Configuration templates


As processing steps/options can significantly differ between beamlines, nabu provides a template system for configuration files.

A template aims at using pre-defined parameters closest to the beamline "daily needs".

Example:

```bash
nabu-config --template id19_pag
```
The available built-in templates are: `id19_pag, id16_holo, id16_ctf`.

You can also create your own template:
  - Edit a configuration file, say `mytemplate.conf`
  - Save it to a dedicated folder, eg. `/home/me/nabu_config_templates/mytemplate.conf`

Then use
```bash
NABU_TEMPLATES_DIR="/home/me/nabu_config_templates" nabu-config --template mytemplate
```



## File format and description

### File format

The `nabu.conf` syntax is similar to the `.ini` files. There are three main entities:

  - Sections
  - `key = value` lines
  - Comments (start with `#`).

For example:
```ini
[preproc]
# Whether to enable flat-field correction
flatfield_enabled = 1
```
the `[preproc]` line indicates that the lines below belong to the section named `preproc`. The line starting with a "`#`" is a comment. Then, the line `flatfield_enabled = 1` indicates that the flat-field normalization will be enabled in the processing.

### Sections

Each section describe a usual processing step. In the current version, the available sections are:

- `dataset`: input dataset description, binning, ...
- `preproc`: phase retrieval, CCD corrections, ...
- `phase`: phase retrieval
- `reconstruction`: tomography reconstruction
- `postproc`: post-processing: histogram
- `output`: output data description
- `resources`: computing resources description
- `pipeline`: options on the processing pipeline

## See also

Page with [all nabu parameters](nabu_config_items.md).

## Notes

### What are the differences with respect to PyHST2 `.par` file ?

The principle is the same: the user defines the processing steps through this configuration file.

However, in Nabu, many things are guessed automatically. For example, you don't need to provide the number of pixels horizontally/vertically in the radios, the length of the numerical part, etc. Basically, all the information that can be automatically inferred from the dataset metadata is automatically handled, so that you only have to fill-in relevant parts.

### Can nabu be used without this configuration file ?

Yes. Nabu is foremost a library, meaning that all its component can be accessed via an Application Programming Interface (API). Please see: [Nabu API reference](apidoc/nabu.rst).

### Compatibility policy

During the development of Nabu, some features will be added, leading to new keys in the configuration file. Besides, some keys might be renamed or even deleted if deemed necessary. 

In any case, **a configuration file from an "old" version of nabu will be supported by newer versions** for some time, with a deprecation warning when using obsolete keys.

