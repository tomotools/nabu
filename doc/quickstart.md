# Quick start

## Create a configuration file

The configuration file defines the different processing steps and their options.
To create a configuration file from scratch, you can use

```bash
nabu-config
```

It will create a file named `nabu.conf` with pre-filled values. More options are available, see `nabu-config --help`.

## Run the reconstruction

Once you filled-in the configuration, just run

```bash
nabu nabu.conf
```

More options are available, see `nabu --help`.  
For example, to reconstruct slices 1000 to 1100: `nabu nabu.conf --slice 1000-1100`



## See also


[Configuration file](nabu_config_file.md)
