# data duplication

the parameter stitching/avoid_data_duplication allow you to activate / deactivate data duplication with some limitation / rules (see below)

```txt
[stitching]
...
avoid_data_duplication = True
```

```{warning}
Data duplication rely on HDF5 mecanism. Nevertheless those are limited. For example we cannot redefine a way to read a dataset (from the right to the left for example). This is why to avoid data duplication you must make sure frame are not flip from one volume to the other.
```

## pre-processing stitching

If stitching is done over projections. As the output provides flat-field normalized projection it will always duplicate data. At the exception of the case where projections of the ![NXtomo](https://manual.nexusformat.org/classes/applications/NXtomo.html) are already normalized and `avoid_data_duplication` is set to True

## post-processing stitching

If stitiching is done over reconstructed volume then in any case the stitching areas between volumes will be created (as this is new).
But remaining area will correspond to raw volumes. And we can avoid copy of this part (for HDF5 input and output volumes only).

If `avoid_data_duplication` is set to True then part corresponding to raw volume will simply be link to original reconstructed volumes.

```{warning}
if `avoid_data_duplication` is True this also mean that stiched reconstructed volume will contain relative links to the reconstruced volumes. So if those are moved / removed or if the stitched volume is moved this mean that link will be break.
```
