# design

All the stitching feature is embed inside nabu stitching module.
To split complexity and test behavior we created the following classes:

* Stitcher classes (:class:`ZStitcher`, :class:`PreProcessZStitcher`, :class:`PostProcessZStitcher`): define the high level API and behavior of the stitcher. Take url as input and volume or NXtomo as output.
* Overlap class (:class:`ZStichOverlapKernel`) class to define overlap between two scans and overlap stratefy classes (:class:`OverlapStichingStrategy`)
* Frame composition utils (:class:`ZFrameComposition`): ease understanding of the stitching.
* utils function:
    * :func:`z_stitch_raw_frames`: raw stitching of several frames
    * :func:`z_stitching`: high level function to apply stitching according to a user defined configuration
