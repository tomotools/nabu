# how to distribute stitching over slurm

You can launch the stitching on a computer having access to the slurm `sbatch` command then you can distribution stitching over slurm.

The stitching configuration file contains a dedicated `slurm` section which allow you to define the distribution to be done.

It contains the following keys:

* `partition`: name of the partition to use for launching the jobs
* `mem`: memory to allocate for each job.
* `n_jobs`: define on how many jobs the stitching should be splitted.
* `python_venv`: python virtual environment to source before applying stitching. It must contain nabu.
* `modules`: modules to be loaded before applying stitching. One must contain nabu. If provided then `python_venv` should be empty. Examples:

    ``` txt
    modules = "tomotools"
    ```

    ``` txt
    modules = ("tomotools/dev", "second_module")
    ```

Distribution will be done as follow (presudo-code):

``` code
compute_shifts [locally or remotly] (option locally will enforce to do this calculation locally)
for each part of the stitching:
    apply_stitching_on_this_part()
concatenate_the_different_part  [locally or remotly] (option locally will enforce to do this calculation locally)
```
