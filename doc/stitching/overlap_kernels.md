# overlap kernels strategies

There is several strategies possible to define how to stich overlaping areas. On the following points you will see a short descrition of each strategy and
an example of how it will stitch images together and 'expected' results.
Here are the 'raw' images used for this example:

![stitching raw data](images/stitch_strategy_raw_data.png)

## mean strategy

It will sum the mean value of the two images

![mean strategy](images/mean_strategy.png)

## closest strategy

It will take the top image for the top part and the bottom image for the bottom part

![closest strategy](images/closest_strategy.png)

## linear weights strategy

It will decrease lineary the weights of the top frame and increase the effect of the bottom frame.

![linear strategy](images/linear_strategy.png)

## cosinus weights strategy

This one is close to the linear weights strategy except that instead of having a linear profile it will have a 'cosinus / sinus' profile and should insure a smoother transition.

![linear strategy](images/cosinus_strategy.png)
