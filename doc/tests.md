# `nabu-test`: running the tests

Nabu comes with a handful of unit tests which can be executed either globally or individually.

## Running all the tests

Once Nabu is installed, the command 

```bash
nabu-test
```
will run all the tests. 

Some tests might be disabled because of missing runtime components. To show the reason why the tests are skipped you can run

```bash
nabu-test -rs
```

The `nabu-test` command inherits from the parameters of `pytest`. Therefore, you can pass any pytest option to this command, ex. `nabu-test -h` for help.

## Running specific tests

Nabu comes with several main modules: `preproc`, `reconstruction`, `cuda`, `opencl`, `misc`. Each module has its tests.

To run the tests related to `preproc`:

```bash
nabu-test preproc
```

To run the tests related to `reconstruction`:
```bash
nabu-test reconstruction
```

To run the tests related to "CCD corrections" in `preproc`:

```bash
nabu-test preproc.test_ccd_corr
```

To run the tests related to FBP in `reconstruction`:
```bash
nabu-test reconstruction.test_fbp
```

You can also combine multiple specific tests. The following example runs all the tests related to `reconstruction`, and the specific tests of Paganin phase retrieval in `preproc`:

```bash
nabu-test reconstruction preproc.test_paganin
```

## Running long tests

Some tests take more time than others, either because the computations intrinsically take more time, or because they need a big amount of data. 

By default, these tests are not run (they are marked as skipped). To enable them, you need to pass the environment variable `NABU_LONG_TESTS=1`:

```bash
NABU_LONG_TESTS=1 nabu-test
```

Some tests might read data from a specific location instead of downloading it. You can provide this location with the environment variable `NABU_BIGDATA_DIR`:

```bash
NABU_BIGDATA_DIR=/tmp/nabu_testdir nabu-test
```

