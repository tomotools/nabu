# Version 2020.5.0


Version 2020.5.0 is a major version aiming at stabilizing fixes and features added over weeks.

```{note}
The changelog is available at https://gitlab.esrf.fr/tomotools/nabu/-/blob/master/CHANGELOG.md
```

## Highlights

This section highlights some of the available [features](features.md).

### New methods for estimating the center of rotation

In the configuration, nabu now provides four methods for estimating the Center Of Rotation (CoR):
  - `centered`: a fast and simple auto-CoR method. It only works when the CoR is not far from the middle of the detector. It does not work for half-tomography.
  - `global`: a slow but robust auto-CoR.
  - `sliding-window`: semi-automatically find the CoR with a sliding window. You have to specify on which side the CoR is (left, center, right).
  - `growing-window` : automatically find the CoR with a sliding-and-growing window. You can tune the option with the parameter 'cor_options'.

Advanced options for each of these methods can be tuned with the `cor_options` parameter.

### Histogram improvements

  - When the slice/volume histogram is computed, the processing is now done on GPU, dramatically speeding up this step.
  - The histogram can now be computed for output file formats other than HDF5.

### Excluding projections

Sometimes it is useful to exclude certain projection images from being processes. It can be done with two different means:
  - The CLI tool `nxtomomill patch-nx` (available since `nxtomomill 0.4.0`) can patch a HDF5-Nexus dataset inplace.
  - In the nabu configuration, you can provide  `exclude_projections = excluded.txt` were the text file contains one radio index to ignore per line.

## Changes

### Unique files `nabu_processes.h5` and `nabu.log`

The files `nabu_processes.h5` (containing various processing steps dumped for further reuse) and `nabu.log` (default file name for logs) are now named `dataset_prefix_nabu_processes.h5` and `dataset_prefix_nabu.log`.

The reason is, sometimes several reconstruction are launched simultaneously on dataset files placed in the same folder, giving conflicts in both files.

### Use of "magnified pixel size"

The HDF5-NX file used as an input contains two pixel sizes: detector pixel size, and "magnified pixel size" accounting for optics effects. Until version `2020.4.1`, the former was taken for scaling the reconstruction and computing the Paganin filter. From `2020.4.1` and on, the magnified pixel size will be used.
This has mainly two effects:
  - For phase retrieval, this change impacts the "blurring effect" but not the gray values scale.
When using a smaller pixel size, the delta/beta value will have to be much smaller to obtain a result similar to the output of previous versions. More precisely, if `magnified_pixel_size = pixel_size/10`, then a result obtained with the parameters `(pixel_size, delta_beta)` using `nabu < 2020.4.1`  can be obtained by using parameters `(magnified_pixel_size, delta_beta/100)` in `nabu >= 2020.4.1`.
  - For reconstruction, this change impacts the final gray values range, as the reconstructions values are normalized with (horizontal) pixel size.
