# Version 2021.2.0

Version 2021.2.0 brings many fixes and new features. 
The API related to pipeline was modified to be more consistent, but it should be still compatible with previous versions.

```{note}
The changelog is available at [https://gitlab.esrf.fr/tomotools/nabu/-/blob/master/CHANGELOG.md](https://gitlab.esrf.fr/tomotools/nabu/-/blob/master/CHANGELOG.md)
```

## Highlights

This section highlights some of the available [features](features.md).

### Full GPU support for the "full radios" pipeline

For the record, nabu comes with two kinds of pipelines:
  - "Chunked": several lines of all projections are read to form sinograms. Adapted to reconstruct a few successive slices (slabs), but also full volumes.
  - "Grouped": projections are processed by groups, using the full image. Inefficient for reconstructing several slices only (as all the volume is read), but required in some cases (eg. CTF pase retrieval).

The latter was introduced in nabu 2021.1.0 and has now full GPU support.

### Support for 3D tiff ("big tiff")

It's now possible to save the reconstructed volume as a single big tiff file. The configuration file options is `tiff_single_file`.
Note that these big tiff files cannot be read natively by all readers, for example Imagej needs an [extension (bio-formats)](https://docs.openmicroscopy.org/bio-formats/5.8.2/users/imagej/installing.html).

The default behavior is still to write individual tiff files for each reconstructed slice.

### New Jpeg2000 options

Output jpeg2000 files can now be tuned with the following options in the configuration file:
  - `jpeg2000_compression_ratio`: define a compression ratio (eg. 10)
  - `float_clip_values`: Lower and upper bounds to use when converting from float32 to int. Floating point values are clipped to these (min, max) values before being cast to integer.


### New FBP options

The FBP reconstruction has two new features, available with the following keys in the configuration file:
  - `clip_outer_circle`: Whether to set to zero voxels falling outside of the reconstruction region (PyHST's `ZEROOFFMASK`)
  - `centered_axis`: If set to true, the reconstructed region is centered on the rotation axis, i.e the center of the image will be the rotation axis position (PyHST's `axis_to_the_center=1`)


## Minor changes

  - Add [nabu-validator](cli_tools) command-line tool
  - Add support for Cuda 11, by changing the way cuda contexts are created/retrieved by nabu.
  - In the configuration file, `flatfield_enabled` becomes `flatfield`. This parameter can have four values:
    - False: flat-field is disabled
    - True: flat-field is enabled, nabu will perform a reduction (eg. mean, median) of successive flats/darks in the dataset.
    - force-compute: re-do the aforementioned reduction even if a hdf5 file containing the result is already available
    - force-load: use the flats/darks inside the provided hdf5 file
  - Half tomography reconstruction: change the way to weight the two half sinograms in the overlapping region. This only impacts the center of the reconstructed slice. The new method is compatible with other existing tomography softwares.
  - Configuration file: ignore unknown keys instead of raising an error


## API

### Pipeline

All the pipeline-specific code was moved to `nabu.pipeline`, including configuration file ingestion.
The rationale is to decouple the individual processing features that can be used alone, from codes related to a more complex pipeline logic.

### Moved modules

  - The sinogram-related functions/classes moved from `preproc` to `reconstruction`.
  - The command-line applications are now in `nabu.app` (`nabu.resource.cli` is no more).


### New library features and API changes

- Cuda classes (eg. Convolution) now use the `CudaProcessing` class with composition, not inheritance, to avoid double inheritance.
- Cuda MedianFilter: add `abs_diff` feature, change API with "cuda_options".
- Add CudaPadding: generic padding methods with Cuda. Supports `["constant", "edge", "reflect", "symmetric", "wrap"]`

- CudaProcessing:
  - Add stream to pass custom Cuda stream
  - `allocate_arrays` and `set_array` are now public methods

- Backprojector:
  - `padding_mode` is now provided in named parameters (instead of `extra_options`)
  - `scale_factor` should now be in `extra_options` (instead of in named parameters)
  - Add `centered_slice` and `clip_outer_circle` extra options

- Change the way cuda contexts are created. Following advice in `pycuda` documentation, Nabu now uses the context obtained by retaining the device's primary context, which is the one used by the CUDA runtime API (ex. scikit-cuda).

- `JP2Writer`: Add `cratios`, `float_clip_values` and `n_threads`.
