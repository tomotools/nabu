__version__ = "2025.1.0-dev9"
__nabu_modules__ = [
    "app",
    "cuda",
    "estimation",
    "io",
    "misc",
    "opencl",
    "pipeline",
    "processing",
    "preproc",
    "reconstruction",
    "resources",
    "thirdparty",
    "stitching",
]
version = __version__
