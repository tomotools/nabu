from ..processing.padding_base import *  # noqa: F403
from ..utils import deprecation_warning

deprecation_warning(
    "nabu.misc.padding has been moved to nabu.processing.padding_base", do_print=True, func_name="padding_base"
)
